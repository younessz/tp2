package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {
    private MutableLiveData<List<Book>> searchResults =
            new MutableLiveData<>();
    private LiveData<List<Book>> allBooks;

    private BookDao BookDao;


    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        BookDao = db.BookDao();
        allBooks = BookDao.getAllBooks();
    }


    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }


    public MutableLiveData<List<Book>> getSearchResults() {
        return searchResults;
    }


    public void insertBook(Book newBook) {
        databaseWriteExecutor.execute(() -> {
            BookDao.insertBook(newBook);
        });
    }


    public void deleteBook(long id) {
        databaseWriteExecutor.execute(() -> {
            BookDao.deleteBook(id);
        });
    }


    public void getBook(long id) {

        Future<List<Book>> fBooks = databaseWriteExecutor.submit(() -> {
            return BookDao.getBook(id);
        });
        try {
            searchResults.setValue(fBooks.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}